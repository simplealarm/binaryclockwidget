/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.sawbinaryclock;

import org.bigfoot.simplealarmwidgets.BaseWidgetUpdater;

import java.util.Calendar;

import android.app.AlarmManager;

import android.appwidget.AppWidgetProvider;
import android.appwidget.AppWidgetManager;

import android.content.Context;
import android.content.res.Resources;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import android.widget.RemoteViews;

public class BinaryClockWidget extends BaseWidgetUpdater
{ 
  private long mLastUpdate = 0; ///< Time of the last update.
  private Bitmap mLastBitmap = null; ///< Bitmap of the last clock.
  private int mLastHour = -1;   ///< Hour of the last update.
  private Bitmap mLastHourBitmap = null;  ///< Image of the clock with only hours.
  
  public BinaryClockWidget()
  {
    this.mName = "Binary Clock";
  }
  
  /** Updates the given views*/
  public RemoteViews getViews(Context context, int appWidgetId, long updateTimeStamp)
  {
    RemoteViews views = new RemoteViews(this.mPkgContext.getPackageName(), R.layout.clockwidget);
    if((updateTimeStamp > mLastUpdate) ||(mLastBitmap == null))
    {
      mLastUpdate = updateTimeStamp;
      mLastBitmap = makeDateTime();
    }
    views.setImageViewBitmap(R.id.clockBitMap, mLastBitmap);
    return views;
  }
  
  public int getClickableView() {return R.id.clockBitMap;}
  
  /** Redraws the binary clock widget
    * @return Binary clock image
    */
  public Bitmap makeDateTime()
  {
    Bitmap outBitmap = null;
    Calendar c = Calendar.getInstance();
    int hours = c.get(Calendar.HOUR_OF_DAY);
    int mins = c.get(Calendar.MINUTE);
    // if hours has changed, rebuild all
    if (hours != mLastHour)
    {
      outBitmap = makeDateTime(hours, mins);
    }
    // ... redraw only minutes otherwise
    else
    {
      outBitmap = makeMinutes(mins);
    }
    return outBitmap;
  }
  
  /** Redraws minutes bits
    * @param[in] mins: minutes to draw
    * @return Binary clock image
    */
  private Bitmap makeMinutes(int mins)
  {
    Resources res = this.mPkgContext.getResources();
    
    Canvas canvas = null;
    Bitmap outBitmap = null;
    // if mins is odd only draw the less significant bit instead of redrawing all
    if((mins & 1) != 0)
    {
      outBitmap = mLastBitmap;
      canvas = new Canvas(outBitmap);
      drawMinutes(1, canvas, res);
    }
    // ... redraws all minutes otherwise
    else
    {
      outBitmap = mLastHourBitmap.copy(Bitmap.Config.ARGB_8888, true);
      canvas = new Canvas(outBitmap);
      drawMinutes(mins, canvas, res);
    }
    return outBitmap;
  }
  
  /** Draws the clock for the given time
    * @param[in] mins: minutes of the clock
    * @param[in] outCanvas: canvas to draw the clock
    * @param[in] res: resources to get the drawables
    */
  private void drawMinutes(int mins, Canvas outCanvas, Resources res)
  {
    int bit = 1;
    int bitDrawable = 0;
    for(int i = 1; i < 7; ++i)
    {
      if ((mins & bit) != 0)
      {
        bitDrawable = getMinsDrawable(i);
        outCanvas.drawBitmap(BitmapFactory.decodeResource(res, bitDrawable), 0, 0,null);
      }
      bit *= 2;
    }
  }
  
  /** Draws the clock for the given time
    * @param[in] hours: hours of the clock
    * @param[in] mins: minutes of the clock
    * @return Image of the binary clock
    */
  private Bitmap makeDateTime(int hours, int mins)
  {
    Resources res = this.mPkgContext.getResources();
    Bitmap container = BitmapFactory.decodeResource(res, R.drawable.light_container).
                          copy(Bitmap.Config.ARGB_8888 ,true);
    Canvas canvas = new Canvas(container);
    
    int bit = 1;
    int bitDrawable = 0;
    for(int i = 1; i < 6; ++i)
    {
      if ((hours & bit) != 0)
      {
        bitDrawable = getHoursDrawable(i);
        canvas.drawBitmap(BitmapFactory.decodeResource(res, bitDrawable), 0, 0,null);
      }
      bit *= 2;
    }
    mLastHourBitmap = container.copy(Bitmap.Config.ARGB_8888, true);
    mLastHour = hours;
    
    drawMinutes(mins, canvas, res);
    return container;
  }
  
  /** Gets the drawable for the given minute bit.
    * @param[in] bitIndex: bit index of the minutes.
    * @return resource drawable of the minutes bit.
    */
  private int getMinsDrawable(int bitIndex)
  {
    int bitDrawable = 0;
    switch(bitIndex)
    {
      case 1:
        bitDrawable = R.drawable.light_01;
        break;
      case 2:
        bitDrawable = R.drawable.light_02;
        break;
      case 3:
        bitDrawable = R.drawable.light_03;
        break;
      case 4:
        bitDrawable = R.drawable.light_04;
        break;
      case 5:
        bitDrawable = R.drawable.light_05;
        break;
      default:
        bitDrawable = R.drawable.light_06;
    }
    return bitDrawable;
  }
  
  /** Gets the drawable for the given hours bit.
    * @param[in] bitIndex: bit index of the hours.
    * @return resource drawable of the hours bit.
    */
  private int getHoursDrawable(int bitIndex)
  {
    int bitDrawable = 0;
    switch(bitIndex)
    {
      case 1:
        bitDrawable = R.drawable.light_10;
        break;
      case 2:
        bitDrawable = R.drawable.light_20;
        break;
      case 3:
        bitDrawable = R.drawable.light_30;
        break;
      case 4:
        bitDrawable = R.drawable.light_40;
        break;
      default:
        bitDrawable = R.drawable.light_50;
    }
    return bitDrawable;
  }
  
  /** Registers the next widget update in the alarm manager*/
  public long getNextUpdateMillis(Context context, int appWidgetId)
  {
    Calendar c = Calendar.getInstance();
    c.add(Calendar.MINUTE, 1);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTimeInMillis();
  }
}

