/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.sawbinaryclock;

import org.bigfoot.simplealarmwidgets.PluginReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class WidgetReceiver extends PluginReceiver
{
  @Override
  public void onReceive(Context context, Intent intent)
  {
  }
  
  @Override
  public PluginReceiver.UpdaterDescriptor[] getWidgetUpdaters()
  {
    PluginReceiver.UpdaterDescriptor[] updaters = new PluginReceiver.UpdaterDescriptor[1];
    updaters[0] = new PluginReceiver.UpdaterDescriptor(
      BinaryClockWidget.class.getName(), new BinaryClockWidget().getName());
    return updaters;
  }
}
